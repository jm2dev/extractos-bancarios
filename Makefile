help:           ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build:	## Build project
	sbt clean "extractosCore/test" "extractosStream/test"

balance=8160845
crunch=feb19.csv

setup:	## Create data folder
	mkdir crunch
	@echo "provide crunch/bank.csv"

dos2unix:	## File to unix format
	dos2unix crunch/bank.csv

run:	## Run program
	sbt "extractosStream/run $(balance) $(amount)"

.header:	## Put header in results
	@echo "Balance,Date,Account,Amount,Subcategory,Memo" >> crunch/$(crunch) 

show:	.header	## Display results
	cat crunch/crunch.csv >> crunch/$(crunch);\
	cat crunch/$(crunch)

