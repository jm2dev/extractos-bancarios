import sbt._

object Dependencies {
  lazy val fs2Core = "co.fs2" %% "fs2-core" % "1.0.1"
  lazy val fs2Io = "co.fs2" %% "fs2-io" % "1.0.1"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
}
