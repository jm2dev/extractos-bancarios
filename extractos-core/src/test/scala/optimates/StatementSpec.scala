package optimates

import org.scalatest.{ FlatSpec, Matchers }
import org.scalatest.prop.TableDrivenPropertyChecks

class StatementSpec extends FlatSpec with Matchers with TableDrivenPropertyChecks {
  behavior of "Balance"

  it should "populate balance from string" in {
    val balances = Table(
      ("actual", "expected"),
      (" ", 0),
      ("null", 0),
      ("-702.00", -70200),
      ("0.65", 65)
    )

    forAll (balances) { 
      (actual: String, expected: Int) => 
      Statement.parseToCents(actual) shouldBe expected
    }
  }

  it should "format balance in cents to pounds" in {
    val balances = Table(
      ("cents", "pounds"),
      (-100, "-1.00"),
      (12345, "123.45")
    )

    forAll (balances) {
      (cents: Int, pounds: String) => 
      Statement.centsToPounds(cents) shouldBe pounds
    }
  }

  behavior of "Statement"

  it should "create statement from string CSV line" in {
    val line = " ,14/08/2017,XX-XX-XX XXXXXXXX,-9.56,PAYMENT,BISTRO BOHEM          SWEDEN                "
    val expected = Statement(
      balance = 0,
      date = "14/08/2017",
      account = "XX-XX-XX XXXXXXXX",
      amount = -956,
      subcategory = "PAYMENT",
      memo = "BISTRO BOHEM          SWEDEN                "
    )

    Statement.parse(line) shouldBe expected
  }

  it should "create CSV line from Statement data" in {
    val statement = Statement(
      balance = 0,
      date = "14/08/2017",
      account = "XX-XX-XX XXXXXXXX",
      amount = -956,
      subcategory = "PAYMENT",
      memo = "BISTRO BOHEM          SWEDEN                "
    )
    val expected = "0.00,14/08/2017,XX-XX-XX XXXXXXXX,-9.56,PAYMENT,BISTRO BOHEM          SWEDEN                "

    Statement.toCsv(statement) shouldBe expected
  }

  it should "calculate balance for statments with no balance" in {
    val seed = Statement(
        7011947,
        "16/11/2018",
        "XX-XX-XX XXXXXXXX",
        -990000,
        "PAYMENT","SOME RANDOM          7428200722A00110A  BBP"
      )

    val input = List(
      Statement(
        0,
        "13/11/2018",
        "XX-XX-XX XXXXXXXX",
        -109900,
        "OTH",
        "APPLE STORE R092      ON 12 NOV          BDC"
      ),
      Statement(
        0,
        "12/11/2018",
        "XX-XX-XX XXXXXXXX",
        -114900,
        "OTH",
        "APPLE STORE R245      ON 11 NOV          BDC"
      )
    )

    val expected = List(
      Statement(
        7011947,
        "16/11/2018",
        "XX-XX-XX XXXXXXXX",
        -990000,
        "PAYMENT","SOME RANDOM          7428200722A00110A  BBP"
      ),
      Statement(
        8001947,
        "13/11/2018",
        "XX-XX-XX XXXXXXXX",
        -109900,
        "OTH",
        "APPLE STORE R092      ON 12 NOV          BDC"
      ),
      Statement(
        8111847,
        "12/11/2018",
        "XX-XX-XX XXXXXXXX",
        -114900,
        "OTH",
        "APPLE STORE R245      ON 11 NOV          BDC"
      )
    )

    input.scan(seed)(Statement.balances) shouldBe expected
  }
}
