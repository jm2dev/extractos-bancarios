package optimates

object Statement {
  val parseToCents = (balance: String) => balance match {
    case " " | "null" => 0
    case str if (str.matches("[-]{0,1}[0-9]+.[0-9]{2}")) => (str.toDouble * 100).toInt
  }

  val parse = (line: String) => {
    val data = line.split(",")
    Statement(
      parseToCents(data(0)), 
      data(1),
      data(2),
      parseToCents(data(3)), 
      data(4),
      data(5)
    )
  }
  val centsToPounds = (cents: Int) => {
    val pounds = cents.toDouble / 100
    f"$pounds%1.2f"
  }

  val toCsv = (stmt: Statement) => s"${centsToPounds(stmt.balance)},${stmt.date},${stmt.account},${centsToPounds(stmt.amount)},${stmt.subcategory},${stmt.memo}"

  val balances = (x: Statement, y: Statement) => 
  Statement(
    x.balance - x.amount, 
    y.date,
    y.account,
    y.amount,
    y.subcategory,
    y.memo
  )
}

case class Statement(
  balance: Int,
  date: String,
  account: String,
  amount: Int,
  subcategory: String,
  memo: String
)
