package optimates

import cats.effect.{ExitCode, IO, IOApp, Resource}
import cats.implicits._
import fs2.{io, text, Stream}
import java.nio.file.Paths
import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext

object Converter extends IOApp {
  private val blockingExecutionContext =
    Resource.make(IO(ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(2))))(ec => IO(ec.shutdown()))

  def converter(seed: Statement): Stream[IO, Unit] = Stream.resource(blockingExecutionContext).flatMap { blockingEC =>

    io.file.readAll[IO](Paths.get("crunch/bank.csv"), blockingEC, 4096)
      .through(text.utf8Decode)
      .through(text.lines)
      .filter(s => !s.trim.isEmpty && !s.startsWith("Number"))
      .map(line => Statement.parse(line))
      .scan(seed)(Statement.balances)
      .map(sencillo => Statement.toCsv(sencillo))
      .filter(s => !s.endsWith("JM"))
      .intersperse("\n")
      .through(text.utf8Encode)
      .through(io.file.writeAll(Paths.get("crunch/crunch.csv"), blockingEC))
  }
  
  def run(args: List[String]): IO[ExitCode] = {
    val balance = args(0).toInt
    val seed = Statement(
      balance,
      "00/00/0000",
      "XX-XX-XX XXXXXXXX",
      0,
      "sub category",
      "memo to mark delete this JM"
    )
    converter(seed).compile.drain.as(ExitCode.Success)
  }
}

