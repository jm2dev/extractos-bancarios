import Dependencies._

ThisBuild / scalaVersion     := "2.12.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.optimates"
ThisBuild / organizationName := "optimates"

lazy val root = (project in file("."))
  .settings(
    name := "Extractos Bancarios",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )

lazy val extractosCore = (project in file("extractos-core"))
  .settings(
    name := "Extractos Bancarios Core",
    libraryDependencies ++= Seq(
      scalaTest % Test
    )
  )

lazy val extractosStream = (project in file("extractos-stream"))
  .settings(
    name := "Extractos Bancarios Stream",
    libraryDependencies ++= Seq(
      fs2Core,
      fs2Io,
      scalaTest % Test
    )
  ).dependsOn(extractosCore)

// Uncomment the following for publishing to Sonatype.
// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for more detail.

// ThisBuild / description := "Some descripiton about your project."
// ThisBuild / licenses    := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
// ThisBuild / homepage    := Some(url("https://github.com/example/project"))
// ThisBuild / scmInfo := Some(
//   ScmInfo(
//     url("https://github.com/your-account/your-project"),
//     "scm:git@github.com:your-account/your-project.git"
//   )
// )
// ThisBuild / developers := List(
//   Developer(
//     id    = "Your identifier",
//     name  = "Your Name",
//     email = "your@email",
//     url   = url("http://your.url")
//   )
// )
// ThisBuild / pomIncludeRepository := { _ => false }
// ThisBuild / publishTo := {
//   val nexus = "https://oss.sonatype.org/"
//   if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
//   else Some("releases" at nexus + "service/local/staging/deploy/maven2")
// }
// ThisBuild / publishMavenStyle := true
